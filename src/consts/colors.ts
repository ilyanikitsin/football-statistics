type NewObj = {
  [key: string]: string;
};

type Colors = {
  [key: string]: NewObj;
};

export const colors: Colors = {
  light: {
    primary: '#ecedf1',
    secondary: '#262626',
    articleText: '#262626',
    green: '#008000',
    buttonBackground: '#008000',
    buttonText: '#ffffff',
    itemBackground: '#ffffff',
    bottomTabBar: '#008000',
    statusBarColor: '#008000',
    headerPrimary: '#008000',
    white: '#fff',
    itemCard: '#2E972E',
    cardHeader: '#5CAE5C',
    accordionContent: '#ffffff',
    accordionHeader: '#5CAE5C',
  },
  dark: {
    primary: '#0a0a0a',
    secondary: '#e7b833',
    articleText: '#ffffff',
    green: '#008000',
    buttonBackground: '#19191b',
    buttonText: '#ffffff',
    itemBackground: '#19191b',
    bottomTabBar: '#282627',
    statusBarColor: '#282627',
    headerPrimary: '#252525',
    white: '#fff',
    itemCard: '#252525',
    cardHeader: '#282627',
    accordionContent: '#e7b833',
    accordionHeader: '#ffffff',
  },
};

export const spinnerColors: NewObj = {
  primary: '#008000',
};
