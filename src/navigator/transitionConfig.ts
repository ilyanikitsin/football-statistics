import { Animated, Easing } from 'react-native';

let SlideFromRight = (index: number, position: any, width: number) => {
  const translateX = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [width, 0, 0],
  });
  return { transform: [{ translateX }] };
};

const TransitionConfiguration = () => {
  return {
    transitionSpec: {
      duration: 350,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: (sceneProps: any) => {
      const { layout, position, scene } = sceneProps;
      const width = layout.initWidth;
      const { index, route } = scene;
      const params = route.params || {};
      const transition = params.transition || 'default';
      // @ts-ignore
      return {
        default: SlideFromRight(index, position, width),
      }[transition];
    },
  };
};

export default TransitionConfiguration;
