import { createStackNavigator } from 'react-navigation-stack';
import { LOGIN_APP, SIGN_UP } from '../consts/screenNames';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';
import TransitionConfiguration from './transitionConfig';

const LoginStack = createStackNavigator(
  {
    [LOGIN_APP]: {
      screen: LoginScreen,
      navigationOptions: {
        header: null,
      },
    },
    [SIGN_UP]: {
      screen: SignUpScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: LOGIN_APP,
    transitionConfig: TransitionConfiguration,
  },
);

export default LoginStack;
