import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { APP, LOGIN_APP, LOGIN_LOADING } from '../consts/screenNames';
import LoginLoadingScreen from '../screens/LoginLoadingScreen';
import LoginStack from './loginNavigator';
import TabNavigator from './tabNavigator';

export default createAppContainer(
  createSwitchNavigator(
    {
      [LOGIN_LOADING]: LoginLoadingScreen,
      [LOGIN_APP]: LoginStack,
      [APP]: TabNavigator,
    },
    {
      initialRouteName: LOGIN_LOADING,
    },
  ),
);
