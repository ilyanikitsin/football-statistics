import { createStackNavigator } from 'react-navigation-stack';
import { COMPETITIONS } from '../consts/screenNames';
import CompetitionsScreen from '../screens/CompetitionsScreen';
import React from 'react';
import TransitionConfiguration from './transitionConfig';
import CompetitionsScreenHeader from '../components/CompetitionsScreenHeader';
import CompetitionInfoScreen from '../screens/CompetitionInfoScreen';
import CompetitionInfoScreenHeader from '../components/CompetitionInfoScreenHeader';

const CompetitionsStack = createStackNavigator(
  {
    [COMPETITIONS]: {
      screen: CompetitionsScreen,
      navigationOptions: (props) => {
        return {
          header: () => <CompetitionsScreenHeader {...props} />,
        };
      },
    },
    Competition: {
      screen: CompetitionInfoScreen,
      navigationOptions: (props) => {
        return {
          header: () => <CompetitionInfoScreenHeader {...props} />,
          gesturesEnabled: true,
          gestureDirection: 'normal',
        };
      },
    },
  },
  {
    initialRouteName: COMPETITIONS,
    transitionConfig: TransitionConfiguration,
  },
);

export default CompetitionsStack;
