import { withNavigation } from 'react-navigation';
import {
  MaterialTopTabBar,
  createMaterialTopTabNavigator,
} from 'react-navigation-tabs';

import TeamPlayersScreen from '../screens/TeamPlayersScreen';
import TeamMatchesScreen from '../screens/TeamMatchesScreen';
import TeamInfoScreen from '../screens/TeamInfoScreen';
import { colors } from '../consts/colors';
import React from 'react';

const TeamTopTabNavigator = createMaterialTopTabNavigator(
  {
    Info: {
      screen: withNavigation(TeamInfoScreen),
    },
    Players: {
      screen: withNavigation(TeamPlayersScreen),
    },
    Matches: {
      screen: withNavigation(TeamMatchesScreen),
    },
  },
  {
    tabBarComponent: (props) => {
      const isColorMode = props.screenProps ? colors.dark : colors.light;

      return (
        <MaterialTopTabBar
          {...props}
          style={{ backgroundColor: isColorMode.bottomTabBar }}
        />
      );
    },
    tabBarOptions: {
      activeTintColor: '#ffffff',
      showLabel: true,
    },
    swipeEnabled: true,
  },
);

export default TeamTopTabNavigator;
