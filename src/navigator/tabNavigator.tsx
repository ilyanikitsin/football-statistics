import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import TeamStack from './teamNavigator';
import Icon from 'react-native-vector-icons/FontAwesome';
import CompetitionsStack from './competitionsNavigator';
import React from 'react';
import withTheme from '../components/hocs/withTheme';
import { colors } from '../consts/colors';

type OutherProps = {
  tintColor: string;
  isColorMode: {
    [key: string]: string;
  };
  screenProps: any;
};

const TabBarComponent = (props: any) => <BottomTabBar {...props} />;

const TabNavigator = createBottomTabNavigator(
  {
    Teams: {
      screen: TeamStack,
      navigationOptions: {
        tabBarIcon: (props: OutherProps) => {
          const { tintColor } = props;

          return <Icon size={24} name="users" color={tintColor} />;
        },
      },
    },
    Competitions: {
      screen: CompetitionsStack,
      navigationOptions: {
        tabBarIcon: (props: OutherProps) => {
          const { tintColor } = props;

          return <Icon size={24} name="trophy" color={tintColor} />;
        },
      },
    },
  },
  {
    tabBarComponent: (props) => {
      const isColorMode = props.screenProps ? colors.dark : colors.light;

      return (
        <TabBarComponent
          {...props}
          style={{ backgroundColor: isColorMode.bottomTabBar }}
        />
      );
    },
    tabBarOptions: {
      activeTintColor: '#ffffff',
      showLabel: false,
    },
  },
);

export default withTheme(TabNavigator);
