import React from 'react';
import { withNavigation } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import TransitionConfiguration from './transitionConfig';

import TeamsScreen from '../screens/TeamsScreen';
import PlayerInfoScreen from '../screens/PlayerInfoScreen';
import SettingsScreen from '../screens/SettingsScreen';

import TeamsScreenHeader from '../components/TeamsScreenHeader';
import SettingsScreenHeader from '../components/SettingsScreenHeader';
import TeamPlayersScreenHeader from '../components/TeamPlayersScreenHeader';
import PlayerInfoScreenHeader from '../components/PlayerInfoScreenHeader';
import TeamTopTabNavigator from './teamTopNavigator';

const TeamStack = createStackNavigator(
  {
    Teams: {
      screen: withNavigation(TeamsScreen),
      navigationOptions: (props) => {
        return {
          header: () => <TeamsScreenHeader {...props} />,
          gesturesEnabled: true,
          gestureDirection: 'normal',
        };
      },
    },
    TeamTopTabNavigator: {
      screen: TeamTopTabNavigator,
      navigationOptions: (props) => {
        return {
          header: () => <TeamPlayersScreenHeader {...props} />,
          gesturesEnabled: true,
          gestureDirection: 'normal',
        };
      },
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: (props) => {
        return {
          header: () => <SettingsScreenHeader {...props} />,
          gesturesEnabled: true,
          gestureDirection: 'normal',
        };
      },
    },
    Player: {
      screen: PlayerInfoScreen,
      navigationOptions: (props) => {
        return {
          header: () => <PlayerInfoScreenHeader {...props} />,
          gesturesEnabled: true,
          gestureDirection: 'normal',
        };
      },
    },
  },
  {
    transitionConfig: TransitionConfiguration,
    navigationOptions: {
      gesturesEnabled: true,
    },
  },
);

export default TeamStack;
