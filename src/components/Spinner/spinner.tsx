import React from 'react';
import { ActivityIndicator } from 'react-native';
import Modal from '../ModalView';
import styles from './styles';

const defaultProps = {
  isVisible: false,
  color: 'green',
  type: 'ACTIVITY_INDICATOR_SPINNER',
};

const Spinner = (props: any) => {
  const { isVisible, color } = { ...defaultProps, ...props };

  return (
    <Modal
      isVisible={isVisible}
      showClose={false}
      modalStyle={styles.containerForActivityIndicator}>
      <ActivityIndicatorSpinner color={color} />
    </Modal>
  );
};

const ActivityIndicatorSpinner = (props: any) => {
  const { color } = props;

  return <ActivityIndicator size={'large'} color={color} />;
};

export default Spinner;
