import React from 'react';
import { Platform, View } from 'react-native';
import { NavigationScreenProp, withNavigation } from 'react-navigation';
import { Container, BackButton } from './styles';
import withTheme from '../hocs/withTheme';
import { ComponentName, ComponentNameText } from './styles';

type OuterProps = {
  navigation: NavigationScreenProp<any, any>;
  isColorMode: {
    [key: string]: string;
  };
};
const SettingsScreenHeader = (props: OuterProps) => {
  const { navigation, isColorMode } = props;

  const onBackPress = () => {
    navigation.goBack();
  };

  return (
    <Container theme={isColorMode}>
      <View>
        <BackButton
          size={24}
          name={Platform.select({
            ios: 'arrow-left',
            android: 'arrow-left',
          })}
          color={isColorMode.white}
          onPress={onBackPress}
        />
      </View>
      <ComponentName theme={isColorMode}>
        <ComponentNameText theme={isColorMode}>Settings</ComponentNameText>
      </ComponentName>
    </Container>
  );
};

export default withTheme(withNavigation(SettingsScreenHeader));
