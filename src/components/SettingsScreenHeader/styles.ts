// @ts-ignore
import styled from 'styled-components/native';
import { colors } from '../../consts/colors';
import Icon from 'react-native-vector-icons/FontAwesome';

export const Container = styled.View`
  padding-top: 10;
  padding-bottom: 10;
  flex-direction: row;
  justify-content: space-between;
  background: ${(props: any) =>
    props.theme.headerPrimary || colors.light.headerPrimary};
`;

export const BackButton = styled(Icon)`
  padding-left: 15;
`;

export const ComponentName = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  width: 100%;
  padding-left: 10px;
`;

export const ComponentNameText = styled.Text`
  color: ${(props: any) => props.theme.white};
  font-size: 20;
`;
