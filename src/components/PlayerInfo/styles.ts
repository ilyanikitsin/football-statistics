// @ts-ignore
import styled from 'styled-components/native';
import { List, ListItem } from 'native-base';

export const Container = styled.View`
  flex: 1;
  background-color: ${(props: any) => props.theme.primary};
  align-items: center;
`;

export const ContainerItems = styled(List)`
  width: 100%;
  background-color: ${(props: any) => props.theme.itemBackground};
  margin-bottom: 5px;
`;

export const PlayerName = styled.Text`
  color: ${(props: any) => props.theme.articleText};
  font-size: 18;
  font-weight: bold;
`;

export const PlayerProps = styled.Text`
  color: ${(props: any) => props.theme.articleText};
  font-size: 16;
`;

export const ContainerPlayer = styled(ListItem)`
  padding-bottom: 5px;
`;

export const PlayerDescription = styled.Text`
  color: ${(props: any) => props.theme.secondary};
  font-size: 16;
`;
