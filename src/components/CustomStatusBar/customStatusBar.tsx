import React from 'react';
import { StatusBar, View } from 'react-native';
import withTheme from '../hocs/withTheme';
import styles from './styles';

type OuterProps = {
  isColorMode: {
    [key: string]: string;
  };
};

const CustomStatusBar = (props: OuterProps) => {
  const { isColorMode } = props;

  return (
    <View style={[styles.statusBar]}>
      <StatusBar translucent backgroundColor={isColorMode.statusBarColor} />
    </View>
  );
};

export default withTheme(CustomStatusBar);
