import { StyleSheet, Platform, StatusBar } from 'react-native';

const statusbarHeight = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default StyleSheet.create({
  statusBar: {
    height: statusbarHeight,
  },
});
