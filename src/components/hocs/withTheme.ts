import { connect } from 'react-redux';
import { colors } from '../../consts/colors';
import { switchValue } from '../../redux/actions';

const mapStateToProps = (state: any) => {
  return {
    darkTheme: state.userReducer.switchValue,
    isColorMode: state.userReducer.switchValue ? colors.dark : colors.light,
  };
};

export default connect(
  mapStateToProps,
  { switchValue },
);
