import React from 'react';
import { useSelector } from 'react-redux';
import { appLoadingSelector } from '../../selectors/common';

import Spinner from '../Spinner/spinner';
import { spinnerColors } from '../../consts/colors';

const CustomSpinner = (props: any) => {
  const isLoading = useSelector(appLoadingSelector);
  const { color = spinnerColors.primary } = props;

  return <Spinner isVisible={isLoading} color={color} />;
};

export default CustomSpinner;
