// @ts-ignore
import styled from 'styled-components/native';
import { TouchableWithoutFeedback } from 'react-native';

export const Container = styled.View`
  flex: 1;
  background-color: ${(props: any) => props.theme.primary};
  align-items: center;
`;

export const ContainerItems = styled.View`
  width: 100%;
  background-color: ${(props: any) => props.theme.itemBackground};
  margin-bottom: 5px;
`;

export const SettingsItemText = styled.Text`
  color: ${(props: any) => props.theme.articleText};
  font-size: 16;
`;

export const SettingsButtonText = styled.Text`
  color: ${(props: any) => props.theme.buttonText};
  font-size: 16;
`;

export const ContainerButtonText = styled.View`
  color: ${(props: any) => props.theme.buttonText};
  background-color: ${(props: any) => props.theme.buttonBackground};
  align-items: center;
  justify-content: center;
  height: 45;
  width: 300;
`;
