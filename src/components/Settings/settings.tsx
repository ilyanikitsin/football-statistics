import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { TouchableWithoutFeedback } from 'react-native';
import { Body, ListItem, Right, Switch } from 'native-base';
import { logoutApp, switchValue } from '../../redux/actions';
import auth from '@react-native-firebase/auth';

import { LOGIN_LOADING } from '../../consts/screenNames';

import {
  Container,
  SettingsItemText,
  ContainerItems,
  ContainerButtonText,
  SettingsButtonText,
} from './styles';

const Settings = (props: any) => {
  const dispatch = useDispatch();
  const { theme, navigation } = props;

  const switch_value = useSelector((state: any) => {
    return state.userReducer.switchValue;
  });

  const onValueChange = (value: boolean) => {
    return dispatch(switchValue(value));
  };

  const logoutButton = () => {
    auth().signOut();
    navigation.navigate(LOGIN_LOADING);
  };

  return (
    <Container theme={theme}>
      <ContainerItems theme={theme}>
        <ListItem icon>
          <Body>
            <SettingsItemText theme={theme}>Dark Mode</SettingsItemText>
          </Body>
          <Right>
            <Switch value={switch_value} onValueChange={onValueChange} />
          </Right>
        </ListItem>
      </ContainerItems>
      <TouchableWithoutFeedback onPress={() => logoutButton()}>
        <ContainerButtonText theme={theme}>
          <SettingsButtonText theme={theme}>Logout</SettingsButtonText>
        </ContainerButtonText>
      </TouchableWithoutFeedback>
    </Container>
  );
};

export default withNavigation(Settings);
