import React from 'react';
import { Platform } from 'react-native';
import { NavigationScreenProp, withNavigation } from 'react-navigation';
import {
  Container,
  RightBarContainer,
  SettingsButton,
  ComponentName,
  ComponentNameText,
} from './styles';
import withTheme from '../hocs/withTheme';

type OuterProps = {
  navigation: NavigationScreenProp<any, any>;
  isColorMode: {
    [key: string]: string;
  };
};
const TeamsScreenHeader = (props: OuterProps) => {
  const { navigation, isColorMode } = props;

  return (
    <Container theme={isColorMode}>
      <ComponentName theme={isColorMode}>
        <ComponentNameText theme={isColorMode}>Teams</ComponentNameText>
      </ComponentName>
      <RightBarContainer>
        <SettingsButton
          size={24}
          name={Platform.select({
            ios: 'cog',
            android: 'cog',
          })}
          color={isColorMode.white}
          onPress={() => {
            navigation.navigate('Settings');
          }}
        />
      </RightBarContainer>
    </Container>
  );
};

export default withTheme(withNavigation(TeamsScreenHeader));
