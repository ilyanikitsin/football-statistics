import React, { useEffect, useState } from 'react';
import { ListItem, Body, Right } from 'native-base';
import { withNavigation } from 'react-navigation';
import { useDispatch, useSelector } from 'react-redux';

import {
  Container,
  ContainerItems,
  CompetitionName,
  CompetitionItemIcon,
} from './styles';

import {
  competitionInfoRequest,
  competitionsRequest,
} from '../../redux/actions';

const CompetitionsList = (props: any) => {
  const dispatch = useDispatch();

  const [refreshing, setRefreshing] = useState(false);

  const handleRefresh = async () => {
    setRefreshing(true);
    await dispatch(competitionsRequest());
    setRefreshing(false);
  };

  const { theme, navigation } = props;

  const competitions = useSelector((state: any) => {
    return state.competitionsReducer.competitions;
  });

  const competitionId = [
    2000,
    2001,
    2002,
    2003,
    2013,
    2014,
    2015,
    2016,
    2017,
    2018,
    2019,
    2021,
  ];

  useEffect(() => {
    if (!competitions.length) {
      dispatch(competitionsRequest());
    }
  }, [dispatch, competitions]);

  const arr = competitions
    .filter((item: any) => competitionId.includes(item.id))
    .slice()
    .sort((a: { name: string }, b: { name: string }) => {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });

  return (
    <Container theme={theme}>
      <ContainerItems
        theme={theme}
        data={arr}
        refreshing={refreshing}
        onRefresh={handleRefresh}
        renderItem={({ item }: any) => (
          <ListItem
            icon
            onPress={() => {
              dispatch(competitionInfoRequest(item.id));
              navigation.navigate('Competition', {
                id: `${item.id}`,
                competition_name: `${item.name}`,
              });
            }}>
            <Body>
              <CompetitionName theme={theme}>{item.name}</CompetitionName>
            </Body>
            <Right>
              <CompetitionItemIcon theme={theme} name="arrow-right" />
            </Right>
          </ListItem>
        )}
        keyExtractor={(item: any) => `${item.id}`}
      />
    </Container>
  );
};

export default withNavigation(CompetitionsList);
