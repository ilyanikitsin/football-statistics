// @ts-ignore
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/FontAwesome';

export const Container = styled.View`
  flex: 1;
  background-color: ${(props: any) => props.theme.primary};
  align-items: center;
`;

export const ContainerItems = styled.FlatList`
  width: 100%;
  background-color: ${(props: any) => props.theme.itemBackground};
  margin-bottom: 5px;
`;

export const CompetitionName = styled.Text`
  color: ${(props: any) => props.theme.articleText};
  font-size: 16;
`;

export const CompetitionItemIcon = styled(Icon)`
  color: ${(props: any) => props.theme.articleText};
  font-size: 16;
`;
