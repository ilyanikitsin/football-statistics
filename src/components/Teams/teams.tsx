import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { teamsRequest } from '../../redux/actions';
import { batch } from 'react-redux';
import { ListItem, Body, Right } from 'native-base';

import {
  teamMatchesRequest,
  teamPlayersRequest,
  teamInfoRequest,
} from '../../redux/actions';

import {
  Container,
  TeamItemIcon,
  ContainerItems,
  TeamItemText,
} from './styles';

const TeamList = (props: any) => {
  const dispatch = useDispatch();
  const { theme, navigation } = props;

  const [refreshing, setRefreshing] = useState(false);

  const handleRefresh = async () => {
    setRefreshing(true);
    await dispatch(teamsRequest());
    setRefreshing(false);
  };

  const teams = useSelector((state: any) => {
    return state.teamsReducer.teams;
  })
    .slice()
    .sort((a: { name: string }, b: { name: string }) => {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });

  const teamInfo = (item: any) => {
    const { id } = item;
    return () => {
      batch(() => {
        dispatch(teamInfoRequest(item, navigation));
        dispatch(teamPlayersRequest(id));
        dispatch(teamMatchesRequest(id));
      });
    };
  };

  return (
    <Container theme={theme}>
      <ContainerItems
        theme={theme}
        data={teams}
        refreshing={refreshing}
        onRefresh={handleRefresh}
        renderItem={({ item }: any) => (
          <ListItem icon onPress={teamInfo(item)}>
            <Body>
              <TeamItemText theme={theme}>{item.name}</TeamItemText>
            </Body>
            <Right>
              <TeamItemIcon name="arrow-right" theme={theme} />
            </Right>
          </ListItem>
        )}
        keyExtractor={(item: any) => `${item.id}`}
      />
    </Container>
  );
};

export default withNavigation(TeamList);
