import React from 'react';
import { Platform, View } from 'react-native';
import { NavigationScreenProp, withNavigation } from 'react-navigation';
import {
  Container,
  RightBarContainer,
  BackButton,
  SettingsButton,
  ComponentNameText,
  ComponentName,
} from './styles';
import withTheme from '../hocs/withTheme';

type OuterProps = {
  navigation: NavigationScreenProp<any, any>;
  isColorMode: {
    [key: string]: string;
  };
};
const TeamPlayersScreenHeader = (props: OuterProps) => {
  const { navigation, isColorMode } = props;

  const team_name = navigation.getParam('team_name');

  const onBackPress = () => {
    navigation.goBack();
  };

  return (
    <Container theme={isColorMode}>
      <View>
        <BackButton
          size={24}
          name={Platform.select({
            ios: 'arrow-left',
            android: 'arrow-left',
          })}
          color={isColorMode.white}
          onPress={onBackPress}
        />
      </View>
      <ComponentName theme={isColorMode}>
        <ComponentNameText theme={isColorMode}>{team_name}</ComponentNameText>
      </ComponentName>
      <RightBarContainer>
        <SettingsButton
          size={24}
          name={Platform.select({
            ios: 'cog',
            android: 'cog',
          })}
          color={isColorMode.white}
          onPress={() => {
            navigation.navigate('Settings');
          }}
        />
      </RightBarContainer>
    </Container>
  );
};

export default withTheme(withNavigation(TeamPlayersScreenHeader));
