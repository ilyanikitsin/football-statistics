// @ts-ignore
import styled from 'styled-components/native';
import { Card, CardItem, Button } from 'native-base';

export const Container = styled.View`
  flex: 1;
  background-color: ${(props: any) => props.theme.primary};
  align-items: center;
`;

export const ContainerItems = styled(Card)`
  width: 100%;
  background-color: ${(props: any) => props.theme.itemBackground};
  margin-bottom: 5px;
  margin-top: 0;
`;

export const TeamName = styled.Text`
  color: ${(props: any) => props.theme.white};
  font-size: 18;
  font-weight: bold;
`;

export const TeamProps = styled.Text`
  color: ${(props: any) => props.theme.white};
  font-size: 16;
`;

export const ContainerItem = styled(CardItem)`
  background-color: ${(props: any) => props.theme.itemCard};
  padding-bottom: 10px;
`;

export const ContainerItemProps = styled(CardItem)`
  background-color: ${(props: any) => props.theme.cardHeader};
  padding-bottom: 5px;
`;

export const IconButton = styled(Button)`
  width: 100;
  height: 100;
`;
