import React from 'react';
import { withNavigation } from 'react-navigation';
import { Body, Left } from 'native-base';
import { useSelector } from 'react-redux';
import IconParser from '../IconParser';

import {
  Container,
  TeamName,
  TeamProps,
  ContainerItems,
  ContainerItem,
  IconButton,
  ContainerItemProps,
} from './styles';

const TeamInfoList = (props: any) => {
  const { theme } = props;

  const team = useSelector((state: any) => {
    return state.teamInfoReducer.team;
  });

  return (
    <Container theme={theme}>
      <ContainerItems theme={theme}>
        <ContainerItem theme={theme}>
          <Left>
            <IconButton>
              <IconParser image={team.crestUrl} />
            </IconButton>
            <Body>
              <TeamName theme={theme}>{team.name}</TeamName>
              <TeamProps theme={theme}>{team.shortName}</TeamProps>
            </Body>
          </Left>
        </ContainerItem>
        <ContainerItemProps theme={theme}>
          <Body>
            <TeamProps theme={theme}>Address: {team.address}</TeamProps>
            <TeamProps theme={theme}>Phone: {team.phone}</TeamProps>
          </Body>
        </ContainerItemProps>
      </ContainerItems>
    </Container>
  );
};

export default withNavigation(TeamInfoList);
