import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withNavigation } from 'react-navigation';
import moment from 'moment';
import { ListItem, Right, Body } from 'native-base';

import { TEAM_MATCHES_FETCH_DATA_REQUEST } from '../../consts/screenNames';

import {
  TeamItemDate,
  TeamItemCompetition,
  ContainerItems,
  Container,
} from './styles';

const TeamMatchList = (props: any) => {
  const { theme, navigation } = props;

  const id = navigation.getParam('id');

  const dispatch = useDispatch();

  const [refreshing, setRefreshing] = useState(false);

  const handleRefresh = async () => {
    setRefreshing(true);
    await dispatch({ type: TEAM_MATCHES_FETCH_DATA_REQUEST, id: id });
    setRefreshing(false);
  };

  const matches = useSelector((state: any) => {
    return state.teamMatchesReducer.matches;
  })
    .slice()
    .sort(
      (
        a: { competition: { utcDate: any } },
        b: { competition: { utcDate: any } },
      ) => {
        if (a.competition.utcDate > b.competition.utcDate) {
          return 1;
        }
        if (a.competition.utcDate < b.competition.utcDate) {
          return -1;
        }
        return 0;
      },
    );

  return (
    <Container theme={theme}>
      <ContainerItems
        theme={theme}
        data={matches}
        refreshing={refreshing}
        onRefresh={handleRefresh}
        renderItem={({ item }: any) => (
          <ListItem icon>
            <Body>
              <TeamItemDate theme={theme}>
                {moment(item.utcDate)
                  .subtract(10, 'days')
                  .calendar()}
              </TeamItemDate>
            </Body>
            <Right>
              <TeamItemCompetition theme={theme}>
                {item.competition.name}
              </TeamItemCompetition>
            </Right>
          </ListItem>
        )}
        keyExtractor={(item: any) => `${item.id}`}
      />
    </Container>
  );
};

export default withNavigation(TeamMatchList);
