import React from 'react';
import { Modal, View, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import TouchableView from '../TouchableView';

const defaultProps = {
  isVisible: false,
  onRequestClose: () => {},
  contentType: 'SMALL_MODAL_VIEW',
  isCloseAnywhere: true,
  closeModal: () => {},
  showClose: true,
  sizeClose: 24,
  colorClose: '#000',
};

const ModalView = (props: any) => {
  const {
    isVisible,
    onRequestClose,
    children,
    contentType,
    isCloseAnywhere,
    closeModal,
    showClose,
    sizeClose,
    colorClose,
    modalStyle,
  } = { ...defaultProps, ...props };

  return (
    <Modal
      transparent={true}
      visible={isVisible}
      onRequestClose={onRequestClose}>
      <TouchableWithoutFeedback
        disabled={!isCloseAnywhere}
        onPress={() => closeModal()}>
        <View style={styles.container}>
          <TouchableWithoutFeedback
            onPress={() => console.log('tap inside modal')}>
            <View
              style={[
                contentType === 'SMALL_MODAL_VIEW'
                  ? styles.smallContainer
                  : styles.bigContainer,
                modalStyle,
              ]}>
              {children}
              <CloseButton
                closeModal={closeModal}
                showClose={showClose}
                size={sizeClose}
                color={colorClose}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const CloseButton = (props: any) => {
  const { closeModal, showClose, size, color } = props;

  if (!showClose) {
    return null;
  }

  return (
    <TouchableView onPress={() => closeModal()} style={styles.closeButton}>
      <Icon name={'times'} size={size} color={color} />
    </TouchableView>
  );
};

export default ModalView;
