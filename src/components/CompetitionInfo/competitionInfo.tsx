import React from 'react';
import { withNavigation } from 'react-navigation';
import {
  ContainerComp,
  CompetitionItemDate,
  ContainerItems,
  CompetitionItemWinner,
} from './styles';

import { useSelector } from 'react-redux';
import { ListItem, Left, Right, Button, Body } from 'native-base';
import moment from 'moment';
import IconParser from '../IconParser';

const CompetitionInfoList = (props: any) => {
  const { theme } = props;

  let { seasons = [] } = useSelector((state: any) => {
    return state.competitionInfoReducer.competition;
  });

  seasons = seasons
    .slice()
    .sort((a: { startDate: any }, b: { startDate: any }) => {
      if (a.startDate > b.startDate) {
        return 1;
      }
      if (a.startDate < b.startDate) {
        return -1;
      }
      return 0;
    });

  return (
    <ContainerComp theme={theme}>
      <ContainerItems
        theme={theme}
        data={seasons}
        renderItem={({ item }: any) => (
          <ListItem icon>
            <Left>
              <Button>
                <IconParser
                  image={
                    item.winner
                      ? item.winner.crestUrl
                      : '../../../assert/team_default_logo.png'
                  }
                />
              </Button>
            </Left>
            <Body>
              <CompetitionItemWinner theme={theme}>
                {'Winner: '}
                {item.winner ? item.winner.shortName : 'Unknown'}
              </CompetitionItemWinner>
            </Body>
            <Right style={{ flexDirection: 'column' }}>
              <CompetitionItemDate theme={theme}>
                {moment(item.startDate)
                  .subtract(10, 'days')
                  .calendar()}
              </CompetitionItemDate>
              <CompetitionItemDate theme={theme}>
                {moment(item.endDate)
                  .subtract(10, 'days')
                  .calendar()}
              </CompetitionItemDate>
            </Right>
          </ListItem>
        )}
        keyExtractor={(item: any) => `${item.id}`}
      />
    </ContainerComp>
  );
};

export default withNavigation(CompetitionInfoList);
