import React from 'react';
// @ts-ignore
import Image from 'react-native-remote-svg';
import styles from './styles';

const IconParser = ({ image }: any) => {
  if (image === '../../../assert/team_default_logo.png') {
    return (
      <Image
        style={styles.icon}
        source={require('../../../assert/team_default_logo.png')}
      />
    );
  }
  if (image) {
    return <Image style={styles.icon} source={{ uri: `${image}` }} />;
  } else {
    return (
      <Image
        style={styles.icon}
        source={require('../../../assert/team_default_logo.png')}
      />
    );
  }
};

export default IconParser;
