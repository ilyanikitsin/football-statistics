import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  icon: {
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
  },
});
