import React, { useState } from 'react';
import { ListItem, Body, Right } from 'native-base';
import { useDispatch, useSelector } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { playerInfoRequest } from '../../redux/actions';

import { PLAYER_INFO_FETCH_DATA_REQUEST } from '../../consts/screenNames';
import { Container, ContainerItems, PlayerName, PlayerRole } from './styles';

const TeamPlayersList = (props: any) => {
  const { theme, navigation } = props;

  const id = navigation.getParam('id');
  const team_name = navigation.getParam('team_name');

  const dispatch = useDispatch();

  const [refreshing, setRefreshing] = useState(false);

  const handleRefresh = async () => {
    setRefreshing(true);
    await dispatch(playerInfoRequest(id));
    setRefreshing(false);
  };

  const players: any = useSelector((state: any) => {
    return state.teamPlayersReducer.squad;
  })
    .slice()
    .sort((a: { name: string }, b: { name: string }) => {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });

  return (
    <Container theme={theme}>
      <ContainerItems
        theme={theme}
        data={players}
        refreshing={refreshing}
        onRefresh={handleRefresh}
        renderItem={({ item }: any) => (
          <ListItem
            icon
            onPress={() => {
              dispatch({ type: PLAYER_INFO_FETCH_DATA_REQUEST, id: item.id });
              navigation.navigate('Player', {
                squad_id: `${item.id}`,
                team_name: `${team_name}`,
                player_name: `${item.name}`,
              });
            }}>
            <Body>
              <PlayerName theme={theme}>{item.name}</PlayerName>
            </Body>
            <Right>
              <PlayerRole theme={theme}>
                {item.position ? item.position : 'Trainer'}
              </PlayerRole>
            </Right>
          </ListItem>
        )}
        keyExtractor={(item: any) => `${item.id}`}
      />
    </Container>
  );
};

export default withNavigation(TeamPlayersList);
