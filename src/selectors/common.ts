import { createSelector } from 'reselect';

export const getAllReducersLoadingStates = (state: any) => {
  return Object.keys(state).reduce((acc: any, item: any) => {
    return state[item].isLoading !== undefined
      ? [...acc, state[item].isLoading]
      : [...acc];
  }, []);
};

export const appLoadingSelector = createSelector(
  [getAllReducersLoadingStates],
  (reducersLoadingStates) => {
    return reducersLoadingStates.some((item: any) => item);
  },
);
