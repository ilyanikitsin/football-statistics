const getApi = (url: string, config = {}): any => {
  return fetch(`http://api.football-data.org${url}`, {
    headers: {
      'Content-Type': 'application/json',
      'X-Auth-Token': 'a74b0b8072d042759f85e8d7460c9923',
    },
    ...config,
  }).then((response) => {
    return response.json();
  });
};

export const getApiTeams = () => {
  return getApi('/v2/teams').then((response: any) => response);
};

export const teamInfo = (id: number) => {
  return getApi(`/v2/teams/${id}`).then((response: any) => response);
};

export const teamMatches = (id: number) => {
  return getApi(`/v2/teams/${id}/matches`).then((response: any) => response);
};

export const teamPlayers = (id: number) => {
  return getApi(`/v2/teams/${id}`).then((response: any) => response);
};

export const playerInfo = (id: number) => {
  return getApi(`/v2/players/${id}`).then((response: any) => response);
};

export const competitions = () => {
  return getApi('/v2/competitions').then((response: any) => response);
};

export const competitionsInfo = (id: number) => {
  return getApi(`/v2/competitions/${id}`).then((response: any) => response);
};
