import React from 'react';
import withTheme from '../../components/hocs/withTheme';
import TeamInfoList from '../../components/TeamInfo';

const TeamInfoScreen = (props: any) => {
  const { isColorMode } = props;

  return <TeamInfoList theme={isColorMode} />;
};

export default withTheme(TeamInfoScreen);
