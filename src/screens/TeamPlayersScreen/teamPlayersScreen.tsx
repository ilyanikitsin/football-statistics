import React from 'react';
import TeamPlayersList from '../../components/TeamPlayers';
import withTheme from '../../components/hocs/withTheme';

const TeamPlayersScreen = (props: any) => {
  const { isColorMode } = props;

  return <TeamPlayersList theme={isColorMode} />;
};

export default withTheme(TeamPlayersScreen);
