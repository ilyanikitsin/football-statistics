import React, { useState } from 'react';
import { ToastAndroid } from 'react-native';
import { useDispatch } from 'react-redux';
import {
  Button,
  Container,
  Content,
  Form,
  Input,
  Item,
  Label,
  Text,
} from 'native-base';
import { withNavigation } from 'react-navigation';

import { teamsRequest } from '../../redux/actions';
import styles from './styles';
import auth from '@react-native-firebase/auth';
import { APP, SIGN_UP } from '../../consts/screenNames';

const LoginScreen = (props: any) => {
  const dispatch = useDispatch();
  const { navigation } = props;

  const [email_value, setEmail] = useState('');
  const [password_value, setPassword] = useState('');

  const handleSignIn = () => {
    auth()
      .signInWithEmailAndPassword(email_value, password_value)
      .then(() => navigation.navigate(APP))
      .catch((e) => ToastAndroid.show(e.message, ToastAndroid.SHORT));
    dispatch(teamsRequest());
  };

  return (
    <Container style={styles.container}>
      <Text style={styles.text_header}>Sign In</Text>
      <Content>
        <Form>
          <Item floatingLabel>
            <Label>Email</Label>
            <Input
              onChangeText={(email) => setEmail(email)}
              value={email_value}
            />
          </Item>
          <Item floatingLabel last>
            <Label>Password</Label>
            <Input
              onChangeText={(password) => setPassword(password)}
              value={password_value}
              secureTextEntry={true}
            />
          </Item>
        </Form>
        <Button style={styles.button_login} onPress={handleSignIn}>
          <Text>Login</Text>
        </Button>
        <Button
          style={styles.button_signUp}
          onPress={() => navigation.navigate(SIGN_UP)}>
          <Text>Don't have an account? Sign Up</Text>
        </Button>
      </Content>
    </Container>
  );
};

export default withNavigation(LoginScreen);
