import React from 'react';
import TeamMatchList from '../../components/TeamMatches';
import withTheme from '../../components/hocs/withTheme';

const TeamMatchesScreen = (props: any) => {
  const { isColorMode } = props;

  return <TeamMatchList theme={isColorMode} />;
};

export default withTheme(TeamMatchesScreen);
