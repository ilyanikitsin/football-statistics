import React from 'react';
import withTheme from '../../components/hocs/withTheme';
import CompetitionInfoList from '../../components/CompetitionInfo';

const CompetitionInfoScreen = (props: any) => {
  const { isColorMode } = props;

  return <CompetitionInfoList theme={isColorMode} />;
};

export default withTheme(CompetitionInfoScreen);
