import React from 'react';
import withTheme from '../../components/hocs/withTheme';
import PlayerInfoList from '../../components/PlayerInfo';

const PlayerInfoScreen = (props: any) => {
  const { isColorMode } = props;

  return <PlayerInfoList theme={isColorMode} />;
};

export default withTheme(PlayerInfoScreen);
