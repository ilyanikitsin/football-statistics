import React from 'react';
import CompetitionsList from '../../components/Competitions';
import withTheme from '../../components/hocs/withTheme';

const CompetitionsScreen = (props: any) => {
  const { isColorMode } = props;

  return <CompetitionsList theme={isColorMode} />;
};

export default withTheme(CompetitionsScreen);
