import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  text_header: {
    marginTop: 20,
    marginBottom: 10,
    fontSize: 25,
  },
  text_underHeader: {
    fontSize: 16,
  },
  button_signUp: {
    backgroundColor: 'green',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 40,
  },
  button_login: {
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  error_text: {
    color: 'red',
  },
});
