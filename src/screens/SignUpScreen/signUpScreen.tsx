import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Button,
  Text,
  Input,
  Container,
  Form,
  Content,
  Item,
  Label,
} from 'native-base';
import { withNavigation } from 'react-navigation';
import auth from '@react-native-firebase/auth';

import styles from './styles';
import { APP, LOGIN_APP } from '../../consts/screenNames';
import { teamsRequest } from '../../redux/actions';
import { ToastAndroid } from 'react-native';
import firebase from 'firebase';

const SignUpScreen = (props: any) => {
  const dispatch = useDispatch();
  const { navigation } = props;

  const [email_value, setEmail] = useState('');
  const [password_value, setPassword] = useState('');
  const [password_confirm_value, setPasswordConfirm] = useState('');

  const handleSignUp = async () => {
    if (
      password_value === password_confirm_value ||
      password_value.length >= 6
    ) {
      auth()
        .createUserWithEmailAndPassword(email_value, password_value)
        .then(() => {
          navigation.navigate(APP);
        })
        .catch((e) => ToastAndroid.show(e.message, ToastAndroid.SHORT));
      dispatch(teamsRequest());
    } else {
      ToastAndroid.show('Pls, confirm your password', ToastAndroid.SHORT);
    }
  };

  return (
    <Container style={styles.container}>
      <Text style={styles.text_header}>Create account</Text>
      <Text style={styles.text_underHeader}>
        It`s free and hardly takes more than 30 seconds
      </Text>
      <Content>
        <Form>
          <Item floatingLabel>
            <Label>Email</Label>
            <Input
              onChangeText={(email) => setEmail(email)}
              value={email_value}
            />
          </Item>
          <Item floatingLabel>
            <Label>Password</Label>
            <Input
              onChangeText={(password) => setPassword(password)}
              value={password_value}
              secureTextEntry={true}
            />
          </Item>
          <Item floatingLabel last>
            <Label>Confirm Password</Label>
            <Input
              onChangeText={(password) => setPasswordConfirm(password)}
              value={password_confirm_value}
              secureTextEntry={true}
            />
          </Item>
        </Form>
        <Button style={styles.button_signUp} onPress={handleSignUp}>
          <Text>Sign Up</Text>
        </Button>
        <Button
          style={styles.button_login}
          onPress={() => navigation.navigate(LOGIN_APP)}>
          <Text>Already have an account? Login</Text>
        </Button>
      </Content>
    </Container>
  );
};

export default withNavigation(SignUpScreen);
