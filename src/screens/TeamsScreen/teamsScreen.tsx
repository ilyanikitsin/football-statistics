import React from 'react';
import TeamList from '../../components/Teams';
import withTheme from '../../components/hocs/withTheme';

const TeamsScreen = (props: any) => {
  const { isColorMode } = props;

  return <TeamList theme={isColorMode} />;
};

export default withTheme(TeamsScreen);
