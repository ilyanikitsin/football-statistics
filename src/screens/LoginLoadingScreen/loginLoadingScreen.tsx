import { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { APP, LOGIN_APP } from '../../consts/screenNames';
import auth from '@react-native-firebase/auth';

const LoginLoadingScreen = (props: any) => {
  const { navigation } = props;

  useEffect(() => {
    auth().onAuthStateChanged((user) => {
      navigation.navigate(user ? APP : LOGIN_APP);
    });
  }, [navigation]);

  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return null;
};

export default LoginLoadingScreen;
