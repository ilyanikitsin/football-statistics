import React from 'react';
import withTheme from '../../components/hocs/withTheme';
import Settings from '../../components/Settings';

const SettingsScreen = (props: any) => {
  const { isColorMode } = props;

  return <Settings theme={isColorMode} />;
};

export default withTheme(SettingsScreen);
