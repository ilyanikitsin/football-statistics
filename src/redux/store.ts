import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';

import AsyncStorage from '@react-native-community/async-storage';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './reducers';
import sagas from './sagas';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const sagaMiddleware = createSagaMiddleware();
const persistedReducer = persistReducer(persistConfig, reducer);

const navigationMiddleware = createReactNavigationReduxMiddleware(
  (state: any) => state.navigatorReducer,
);

let middlewares = applyMiddleware(navigationMiddleware, sagaMiddleware);

const store = createStore(persistedReducer, composeWithDevTools(middlewares));

sagaMiddleware.run(sagas);

const persistor = persistStore(store);

export default { store, persistor };
