import { SWITCHVALUE } from '../../consts/screenNames';

export const switchValue = (value: boolean) => ({
  type: SWITCHVALUE,
  payload: value,
});
