import {
  COMPETITION_INFO_FETCH_DATA_REQUEST,
  COMPETITIONS_FETCH_DATA_REQUEST,
  LOGIN,
  LOGOUT,
  TEAM_INFO_FETCH_DATA_REQUEST,
  TEAM_MATCHES_FETCH_DATA_REQUEST,
  TEAM_PLAYERS_FETCH_DATA_REQUEST,
  TEAMS_FETCH_DATA_REQUEST,
} from '../../consts/screenNames';

export const teamsRequest = () => ({
  type: TEAMS_FETCH_DATA_REQUEST,
});

export const loginApp = (navigation: any) => ({
  type: LOGIN,
  navigation: navigation,
});

export const logoutApp = (navigation: any) => ({
  type: LOGOUT,
  navigation: navigation,
});

export const teamInfoRequest = (item: any, navigation: any) => ({
  type: TEAM_INFO_FETCH_DATA_REQUEST,
  item: item,
  navigation: navigation,
});

export const teamPlayersRequest = (id: number) => ({
  type: TEAM_PLAYERS_FETCH_DATA_REQUEST,
  id: id,
});

export const teamMatchesRequest = (id: number) => ({
  type: TEAM_MATCHES_FETCH_DATA_REQUEST,
  id: id,
});

export const playerInfoRequest = (id: number) => ({
  type: TEAM_PLAYERS_FETCH_DATA_REQUEST,
  id: id,
});

export const competitionsRequest = () => ({
  type: COMPETITIONS_FETCH_DATA_REQUEST,
});

export const competitionInfoRequest = (id: number) => ({
  type: COMPETITION_INFO_FETCH_DATA_REQUEST,
  id: id,
});
