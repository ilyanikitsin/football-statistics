import { put, call } from 'redux-saga/effects';
import {
  TEAM_PLAYERS_DATA_SUCCESS,
  TEAM_PLAYERS_HAS_ERRORED,
} from '../../consts/screenNames';
import { teamPlayers } from '../../services/api';

export function* getTeamPlayers(action: any) {
  try {
    const response = yield call(teamPlayers, action.id);

    if (response.squad !== undefined) {
      yield put({
        type: TEAM_PLAYERS_DATA_SUCCESS,
        payload: response.squad,
      });
    } else {
      throw new Error(response.message);
    }
  } catch (error) {
    yield put({
      type: TEAM_PLAYERS_HAS_ERRORED,
      hasErrored: error.message,
      isError: true,
    });
  }
}
