import { all, takeLatest } from 'redux-saga/effects';

import {
  COMPETITION_INFO_FETCH_DATA_REQUEST,
  COMPETITION_INFO_HAS_ERRORED,
  COMPETITIONS_FETCH_DATA_REQUEST,
  COMPETITIONS_HAS_ERRORED,
  PLAYER_INFO_FETCH_DATA_REQUEST,
  PLAYER_INFO_HAS_ERRORED,
  TEAM_INFO_FETCH_DATA_REQUEST,
  TEAM_INFO_HAS_ERRORED,
  TEAM_MATCHES_FETCH_DATA_REQUEST,
  TEAM_MATCHES_HAS_ERRORED,
  TEAM_PLAYERS_FETCH_DATA_REQUEST,
  TEAM_PLAYERS_HAS_ERRORED,
  TEAMS_FETCH_DATA_REQUEST,
  TEAMS_HAS_ERRORED,
} from '../../consts/screenNames';

import { getTeams } from './teamsActions';
import { getTeamInfo } from './teamInfoActions';
import { getTeamMatches } from './teamMatchesActions';
import { getTeamPlayers } from './teamPlayersActions';
import { getPlayerInfo } from './playerInfoActions';
import { getCompetitions } from './competitionsActions';
import { getCompetitionInfo } from './competitionInfoActions';
import { showToast } from './toastActions';

export default function* root() {
  yield all([
    takeLatest(TEAMS_FETCH_DATA_REQUEST, getTeams),
    takeLatest(TEAMS_HAS_ERRORED, showToast),

    takeLatest(TEAM_INFO_FETCH_DATA_REQUEST, getTeamInfo),
    takeLatest(TEAM_INFO_HAS_ERRORED, showToast),

    takeLatest(TEAM_MATCHES_FETCH_DATA_REQUEST, getTeamMatches),
    takeLatest(TEAM_MATCHES_HAS_ERRORED, showToast),

    takeLatest(TEAM_PLAYERS_FETCH_DATA_REQUEST, getTeamPlayers),
    takeLatest(TEAM_PLAYERS_HAS_ERRORED, showToast),

    takeLatest(PLAYER_INFO_FETCH_DATA_REQUEST, getPlayerInfo),
    takeLatest(PLAYER_INFO_HAS_ERRORED, showToast),

    takeLatest(COMPETITIONS_FETCH_DATA_REQUEST, getCompetitions),
    takeLatest(COMPETITIONS_HAS_ERRORED, showToast),

    takeLatest(COMPETITION_INFO_FETCH_DATA_REQUEST, getCompetitionInfo),
    takeLatest(COMPETITION_INFO_HAS_ERRORED, showToast),
  ]);
}
