import { put, call } from 'redux-saga/effects';
import {
  TEAM_MATCHES_DATA_SUCCESS,
  TEAM_MATCHES_HAS_ERRORED,
} from '../../consts/screenNames';
import { teamMatches } from '../../services/api';

export function* getTeamMatches(action: any) {
  try {
    const response = yield call(teamMatches, action.id);

    if (response.matches !== undefined) {
      yield put({
        type: TEAM_MATCHES_DATA_SUCCESS,
        payload: response.matches,
      });
    } else {
      throw new Error(response.message);
    }
  } catch (error) {
    yield put({
      type: TEAM_MATCHES_HAS_ERRORED,
      hasErrored: error.message,
      isError: true,
    });
  }
}
