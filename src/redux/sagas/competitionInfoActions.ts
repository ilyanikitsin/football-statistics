import { put, call } from 'redux-saga/effects';
import {
  COMPETITION_INFO_DATA_SUCCESS,
  COMPETITION_INFO_HAS_ERRORED,
} from '../../consts/screenNames';
import { competitionsInfo } from '../../services/api';

export function* getCompetitionInfo(action: any) {
  try {
    const response = yield call(competitionsInfo, action.id);

    if (response !== undefined) {
      yield put({
        type: COMPETITION_INFO_DATA_SUCCESS,
        payload: response,
      });
    } else {
      throw new Error('error');
    }
  } catch (error) {
    yield put({
      type: COMPETITION_INFO_HAS_ERRORED,
      hasErrored: error.message,
      isError: true,
    });
  }
}
