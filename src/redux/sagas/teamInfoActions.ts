import { put, call } from 'redux-saga/effects';
import {
  TEAM_INFO_DATA_SUCCESS,
  TEAM_INFO_HAS_ERRORED,
} from '../../consts/screenNames';
import { teamInfo } from '../../services/api';
import { NavigationActions } from 'react-navigation';

export const navigateTeamInfo = ({ id, name }: any) =>
  NavigationActions.navigate({
    params: {
      id: `${id}`,
      team_name: `${name}`,
    },
    routeName: 'TeamTopTabNavigator',
  });

export function* getTeamInfo(action: any) {
  try {
    const { id, name } = action.item;
    const { navigation } = action;
    const response = yield call(teamInfo, id);

    if (response !== undefined) {
      yield put({
        type: TEAM_INFO_DATA_SUCCESS,
        payload: response,
      });
      navigation.dispatch(navigateTeamInfo({ id, name }));
    } else {
      throw new Error(response.message);
    }
  } catch (error) {
    yield put({
      type: TEAM_INFO_HAS_ERRORED,
      hasErrored: error.message,
      isError: true,
    });
  }
}
