import { put, call } from 'redux-saga/effects';
import {
  PLAYER_INFO_DATA_SUCCESS,
  PLAYER_INFO_HAS_ERRORED,
} from '../../consts/screenNames';
import { playerInfo } from '../../services/api';

export function* getPlayerInfo(action: any) {
  try {
    const response = yield call(playerInfo, action.id);

    if (response !== undefined) {
      yield put({
        type: PLAYER_INFO_DATA_SUCCESS,
        payload: response,
      });
    } else {
      throw new Error(response.message);
    }
  } catch (error) {
    yield put({
      type: PLAYER_INFO_HAS_ERRORED,
      hasErrored: error.message,
      isError: true,
    });
  }
}
