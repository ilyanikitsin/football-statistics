import { put, call } from 'redux-saga/effects';
import {
  COMPETITIONS_DATA_SUCCESS,
  COMPETITIONS_HAS_ERRORED,
} from '../../consts/screenNames';
import { competitions } from '../../services/api';

export function* getCompetitions() {
  try {
    const response = yield call(competitions);

    if (response.competitions !== undefined) {
      yield put({
        type: COMPETITIONS_DATA_SUCCESS,
        payload: response.competitions,
      });
    } else {
      throw new Error(response.message);
    }
  } catch (error) {
    yield put({
      type: COMPETITIONS_HAS_ERRORED,
      hasErrored: error.message,
      isError: true,
    });
  }
}
