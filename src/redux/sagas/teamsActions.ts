import { put, call } from 'redux-saga/effects';
import { getApiTeams } from '../../services/api';

import {
  TEAMS_FETCH_DATA_SUCCESS,
  TEAMS_HAS_ERRORED,
} from '../../consts/screenNames';

export function* getTeams() {
  try {
    const response = yield call(getApiTeams);

    if (response.teams !== undefined) {
      yield put({
        type: TEAMS_FETCH_DATA_SUCCESS,
        payload: response.teams,
      });
    } else {
      throw new Error(response.message);
    }
  } catch (error) {
    yield put({
      type: TEAMS_HAS_ERRORED,
      hasErrored: error.message,
      isError: true,
    });
  }
}
