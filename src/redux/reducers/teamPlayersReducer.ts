import {
  LOGOUT,
  TEAM_PLAYERS_DATA_SUCCESS,
  TEAM_PLAYERS_FETCH_DATA_REQUEST,
  TEAM_PLAYERS_HAS_ERRORED,
} from '../../consts/screenNames';

const initialState = {
  squad: [],
  isLoading: false,
  hasErrored: '',
};

export function teamPlayersReducer(state = initialState, action: any) {
  switch (action.type) {
    case TEAM_PLAYERS_HAS_ERRORED: {
      return {
        ...state,
        hasErrored: action.error,
        isLoading: false,
      };
    }

    case TEAM_PLAYERS_FETCH_DATA_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case TEAM_PLAYERS_DATA_SUCCESS: {
      return {
        ...state,
        squad: action.payload,
        isLoading: false,
      };
    }

    case LOGOUT: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}
