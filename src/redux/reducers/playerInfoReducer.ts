import {
  PLAYER_INFO_DATA_SUCCESS,
  PLAYER_INFO_FETCH_DATA_REQUEST,
  PLAYER_INFO_HAS_ERRORED,
  LOGOUT,
} from '../../consts/screenNames';

const initialState = {
  player: {},
  isLoading: false,
  hasErrored: '',
};

export function playerInfoReducer(state = initialState, action: any) {
  switch (action.type) {
    case PLAYER_INFO_HAS_ERRORED: {
      return {
        ...state,
        hasErrored: action.error,
        isLoading: false,
      };
    }

    case PLAYER_INFO_FETCH_DATA_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case PLAYER_INFO_DATA_SUCCESS: {
      return {
        ...state,
        player: action.payload,
        isLoading: false,
      };
    }

    case LOGOUT: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}
