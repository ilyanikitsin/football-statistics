import { SWITCHVALUE } from '../../consts/screenNames';

const initialState = {
  switchValue: false,
};

export function userReducer(state = initialState, action: any) {
  switch (action.type) {
    case SWITCHVALUE: {
      return {
        ...state,
        switchValue: action.payload,
      };
    }

    default:
      return state;
  }
}
