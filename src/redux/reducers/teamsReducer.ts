import {
  LOGOUT,
  TEAMS_FETCH_DATA_REQUEST,
  TEAMS_FETCH_DATA_SUCCESS,
  TEAMS_HAS_ERRORED,
} from '../../consts/screenNames';

const initialState = {
  teams: [],
  isLoading: false,
  hasErrored: '',
};

export function teamsReducer(state = initialState, action: any) {
  switch (action.type) {
    case TEAMS_HAS_ERRORED: {
      return {
        ...state,
        hasErrored: action.error,
        isLoading: false,
      };
    }

    case TEAMS_FETCH_DATA_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case TEAMS_FETCH_DATA_SUCCESS: {
      return {
        ...state,
        teams: action.payload,
        isLoading: false,
      };
    }

    case LOGOUT: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}
