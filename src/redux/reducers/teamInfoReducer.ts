import {
  LOGOUT,
  TEAM_INFO_DATA_SUCCESS,
  TEAM_INFO_FETCH_DATA_REQUEST,
  TEAM_INFO_HAS_ERRORED,
} from '../../consts/screenNames';

const initialState = {
  team: {},
  isLoading: false,
  hasErrored: '',
};

export function teamInfoReducer(state = initialState, action: any) {
  switch (action.type) {
    case TEAM_INFO_HAS_ERRORED: {
      return {
        ...state,
        hasErrored: action.error,
        isLoading: false,
      };
    }

    case TEAM_INFO_FETCH_DATA_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case TEAM_INFO_DATA_SUCCESS: {
      return {
        ...state,
        team: action.payload,
        isLoading: false,
      };
    }

    case LOGOUT: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}
