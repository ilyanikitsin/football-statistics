import {
  LOGOUT,
  TEAM_MATCHES_DATA_SUCCESS,
  TEAM_MATCHES_FETCH_DATA_REQUEST,
  TEAM_MATCHES_HAS_ERRORED,
} from '../../consts/screenNames';

const initialState = {
  matches: [],
  isLoading: false,
  hasErrored: '',
};

export function teamMatchesReducer(state = initialState, action: any) {
  switch (action.type) {
    case TEAM_MATCHES_HAS_ERRORED: {
      return {
        ...state,
        hasErrored: action.error,
        isLoading: false,
      };
    }

    case TEAM_MATCHES_FETCH_DATA_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case TEAM_MATCHES_DATA_SUCCESS: {
      return {
        ...state,
        matches: action.payload,
        isLoading: false,
      };
    }

    case LOGOUT: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}
