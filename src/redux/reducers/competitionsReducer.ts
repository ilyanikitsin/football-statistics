import {
  COMPETITIONS_DATA_SUCCESS,
  COMPETITIONS_FETCH_DATA_REQUEST,
  COMPETITIONS_HAS_ERRORED, LOGOUT,
} from '../../consts/screenNames';

const initialState = {
  competitions: [],
  isLoading: false,
  hasErrored: '',
};

export function competitionsReducer(state = initialState, action: any) {
  switch (action.type) {
    case COMPETITIONS_HAS_ERRORED: {
      return {
        ...state,
        hasErrored: action.error,
        isLoading: false,
      };
    }

    case COMPETITIONS_FETCH_DATA_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case COMPETITIONS_DATA_SUCCESS: {
      return {
        ...state,
        competitions: action.payload,
        isLoading: false,
      };
    }

    case LOGOUT: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}
