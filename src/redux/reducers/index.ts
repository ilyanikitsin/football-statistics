import { combineReducers } from 'redux';
import { teamsReducer } from './teamsReducer';
import { teamMatchesReducer } from './teamMatchesReducer';
import { teamPlayersReducer } from './teamPlayersReducer';
import { competitionsReducer } from './competitionsReducer';
import { userReducer } from './userReducer';
import { playerInfoReducer } from './playerInfoReducer';
import { teamInfoReducer } from './teamInfoReducer';
import { competitionInfoReducer } from './competitionInfoReducer';

export default combineReducers({
  teamsReducer,
  teamMatchesReducer,
  teamPlayersReducer,
  competitionsReducer,
  userReducer,
  playerInfoReducer,
  teamInfoReducer,
  competitionInfoReducer,
});
