import {
  LOGOUT,
  COMPETITION_INFO_HAS_ERRORED,
  COMPETITION_INFO_FETCH_DATA_REQUEST,
  COMPETITION_INFO_DATA_SUCCESS,
} from '../../consts/screenNames';

const initialState = {
  competition: {},
  isLoading: false,
  hasErrored: '',
};

export function competitionInfoReducer(state = initialState, action: any) {
  switch (action.type) {
    case COMPETITION_INFO_HAS_ERRORED: {
      return {
        ...state,
        hasErrored: action.error,
        isLoading: false,
      };
    }

    case COMPETITION_INFO_FETCH_DATA_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case COMPETITION_INFO_DATA_SUCCESS: {
      return {
        ...state,
        competition: action.payload,
        isLoading: false,
      };
    }

    case LOGOUT: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}
