import React, { useState } from 'react';
// @ts-ignore
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import Store from './src/redux/store';
import MainNavigator from './src/navigator/mainNavigator';
import CustomStatusBar from './src/components/CustomStatusBar';
import CustomSpinner from './src/components/customSpinner';
import { spinnerColors } from './src/consts/colors';

const { store, persistor } = Store;

const App = () => {
  const [appState, setAppState] = useState(
    store.getState().userReducer.switchValue,
  );
  store.subscribe(() => {
    setAppState(store.getState().userReducer.switchValue);
  });

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <CustomStatusBar />
        <CustomSpinner color={spinnerColors.primary} />
        <MainNavigator screenProps={appState} />
      </PersistGate>
    </Provider>
  );
};

export default App;
